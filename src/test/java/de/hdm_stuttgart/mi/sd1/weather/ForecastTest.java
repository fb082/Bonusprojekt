package de.hdm_stuttgart.mi.sd1.weather;


import de.hdm_stuttgart.mi.sd1.weather.cities.City;
import de.hdm_stuttgart.mi.sd1.weather.model.Weather;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.*;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ForecastTest {

    @Test
    public void testSearchCities() throws Exception {
        String testCityName = "Stuttgart";

        List<String> expected = new ArrayList<>();
        expected.add("Stadtkreis Stuttgart");
        expected.add("Regierungsbezirk Stuttgart");
        expected.add("Stuttgart");
        expected.add("Stuttgart Feuerbach");
        expected.add("Stuttgart Muehlhausen");
        expected.add("Stuttgart-Ost");
        List<City> result = Forecast.searchCities(testCityName);


        if (expected.size() != result.size()) {
            Assert.fail();
        }
        else {
            for (int i = 0; i < result.size(); i++) {
                String resultText = result.get(i).getName();
                String expectedText = expected.get(i);
                if (!resultText.equals(expectedText)) {
                    Assert.fail();
                }
            }
            Assert.assertTrue(true);
        }



    }

    @Test
    public void testAskForInput() throws Exception {
        int expected = 2825297;
        int result = -1;

        List<City> testList = new ArrayList<>();
        testList = Forecast.searchCities("Stuttgart");
        String testInput = "3";

        // Creates a mockScanner Objecct that simulates an user input
        InputStream stdin = System.in;
        System.setIn(new ByteArrayInputStream(testInput.getBytes()));
        Scanner mockScanner = new Scanner(System.in);

        result = Forecast.askForInput(testList, mockScanner);

        Assert.assertEquals(result, expected);
    }

    @Test
    public void testGetForecast() throws IOException {

        URL url = new URL("https://api.openweathermap.org/data/2.5/forecast?lang=de&APPID=ea93f65b556763b06e4f3aad667042c7&units=metric&id=2172797");
        FileUtils.copyURLToFile(url, new File("src/main/archive/W2172797.json"));

        Weather expected = WeatherDataParser.parseFileToObject("src/main/archive/W2172797.json");
        Weather result = Forecast.getForecast(2172797);

        Assert.assertEquals(expected.getCity().getName(), result.getCity().getName());
    }

    @Test
    public void testPrintForecast() throws IOException, ParseException {

        // Saves every system.out.print text ind consoleOutput object
        ByteArrayOutputStream consoleOutput = new ByteArrayOutputStream();
        System.setOut(new PrintStream(consoleOutput));

        String expected = "\n" +
                "Montag, 2018-06-18\n" +
                "\t18:00 Uhr  20.72°C  Klarer Himmel\n" +
                "\t21:00 Uhr  15.52°C  Klarer Himmel\n" +
                "\n" +
                "Dienstag, 2018-06-19\n" +
                "\t00:00 Uhr  13.08°C  Klarer Himmel\n" +
                "\t03:00 Uhr  11.92°C  Ein paar Wolken\n" +
                "\t06:00 Uhr  17.51°C  Klarer Himmel\n" +
                "\t09:00 Uhr  21.02°C  Ein paar Wolken\n" +
                "\t12:00 Uhr  23.21°C  Klarer Himmel\n" +
                "\t15:00 Uhr  24.25°C  Klarer Himmel\n" +
                "\t18:00 Uhr  22.82°C  Klarer Himmel\n" +
                "\t21:00 Uhr  17.46°C  Klarer Himmel\n" +
                "\n" +
                "Mittwoch, 2018-06-20\n" +
                "\t00:00 Uhr  14.63°C  Klarer Himmel\n" +
                "\t03:00 Uhr  13.16°C  Klarer Himmel\n" +
                "\t06:00 Uhr  18.9°C  Klarer Himmel\n" +
                "\t09:00 Uhr  24.02°C  Klarer Himmel\n" +
                "\t12:00 Uhr  26.24°C  Klarer Himmel\n" +
                "\t15:00 Uhr  27.15°C  Klarer Himmel\n" +
                "\t18:00 Uhr  25.83°C  Klarer Himmel\n" +
                "\t21:00 Uhr  20.32°C  Klarer Himmel\n" +
                "\n" +
                "Donnerstag, 2018-06-21\n" +
                "\t00:00 Uhr  17.05°C  Klarer Himmel\n" +
                "\t03:00 Uhr  15.4°C  Klarer Himmel\n" +
                "\t06:00 Uhr  21.37°C  Klarer Himmel\n" +
                "\t09:00 Uhr  25.76°C  Klarer Himmel\n" +
                "\t12:00 Uhr  25.99°C  Klarer Himmel\n" +
                "\t15:00 Uhr  24.87°C  Klarer Himmel\n" +
                "\t18:00 Uhr  21.74°C  Überwiegend bewölkt\n" +
                "\t21:00 Uhr  16.73°C  Leichter Regen\n" +
                "\n" +
                "Freitag, 2018-06-22\n" +
                "\t00:00 Uhr  14.03°C  Wolkenbedeckt\n" +
                "\t03:00 Uhr  12.16°C  Leichter Regen\n" +
                "\t06:00 Uhr  11.39°C  Leichter Regen\n" +
                "\t09:00 Uhr  13.8°C  Klarer Himmel\n" +
                "\t12:00 Uhr  15.95°C  Klarer Himmel\n" +
                "\t15:00 Uhr  16.95°C  Klarer Himmel\n" +
                "\t18:00 Uhr  15.57°C  Klarer Himmel\n" +
                "\t21:00 Uhr  11.64°C  Klarer Himmel\n" +
                "\n" +
                "Samstag, 2018-06-23\n" +
                "\t00:00 Uhr  8.86°C  Klarer Himmel\n" +
                "\t03:00 Uhr  6.65°C  Klarer Himmel\n" +
                "\t06:00 Uhr  10.98°C  Klarer Himmel\n" +
                "\t09:00 Uhr  14.66°C  Klarer Himmel\n" +
                "\t12:00 Uhr  17.09°C  Klarer Himmel\n" +
                "\t15:00 Uhr  17.86°C  Klarer Himmel\n";

        Weather testWeather = WeatherDataParser.parseFileToObject("src/main/resources/W2825297.json");

        Forecast.printForecast(testWeather);

        Assert.assertEquals(expected, consoleOutput.toString());


    }


}
