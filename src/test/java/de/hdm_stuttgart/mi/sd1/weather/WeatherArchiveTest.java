package de.hdm_stuttgart.mi.sd1.weather;

import de.hdm_stuttgart.mi.sd1.weather.model.Weather;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;

public class WeatherArchiveTest {

    // This id matches the city Cairns in Australia and is used in examples by openweathermap
    public int testID = 2172797;

    /**
     * @author Fabian Brecht
     */
    @Test
    public void testIdToFileName() {
        String expected = "src/main/archive/W" + testID + ".json";

        Assert.assertEquals(expected, WeatherArchive.idToFileName(testID)
        );
    }

    /**
     * @author Fabian Brecht
     */
    @Test
    public void testCheckArchive() {

        try
        {
            File testFile = new File(WeatherArchive.idToFileName(testID));
            Calendar CalExpiryDate = Calendar.getInstance();
            CalExpiryDate.add(Calendar.MINUTE, -10);

            if (!testFile.exists()) {
                boolean result = WeatherArchive.checkArchive(testID);
                Assert.assertFalse(result);
            }
            else if (FileUtils.isFileOlder(testFile, CalExpiryDate.getTime())) {
                boolean result = WeatherArchive.checkArchive(testID);
                Assert.assertFalse(result);
            }
            else {
                boolean result = WeatherArchive.checkArchive(testID);
                Assert.assertTrue(result);
            }
        }
        catch (IOException e)
        {
            Assert.fail(e.getStackTrace().toString());
        }
    }

    /**
     * @author Fabian Brecht
     */
    @Test
    public void updateArchiveFromWeb() {
        try
        {
            File WeatherFile = new File(WeatherArchive.idToFileName(testID));
            if (WeatherFile.exists()) FileUtils.forceDelete(WeatherFile); // Deletes the TestFile so it can be downloaded again

            WeatherArchive.updateArchiveFromWeb(testID);
            Assert.assertTrue(WeatherFile.exists());
        }
        catch (Exception e)
        {
            Assert.fail(e.getStackTrace().toString());
        }

    }

    /**
     * @author Fabian Brecht
     */
    @Test
    public void testRequestfromArchive() {
        try
        {
            String expected = "Cairns";
            Weather result = WeatherArchive.requestFromArchive(testID);
            Assert.assertEquals(result.getCity().getName(), expected);
        }
        catch (Exception e)
        {
            Assert.fail(e.getStackTrace().toString());
        }


    }
}
