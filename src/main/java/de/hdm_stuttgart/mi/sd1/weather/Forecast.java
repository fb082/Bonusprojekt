package de.hdm_stuttgart.mi.sd1.weather;


import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import de.hdm_stuttgart.mi.sd1.weather.cities.Cities;
import de.hdm_stuttgart.mi.sd1.weather.cities.City;
import de.hdm_stuttgart.mi.sd1.weather.model.Weather;
import de.hdm_stuttgart.mi.sd1.weather.model.WeatherData;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Providing terminal based weather forecast
 */
public class Forecast {

    /**
     * <p>Entry starting the application.</p>   * @param args Yet unused.
     */
    public static void main(String[] args) throws Exception {
        String userInput = "-1";
        List<City> results = new ArrayList<City>();

        if (args.length > 0) {
            userInput = args[0]; // Get user input from String array args
        }

        results = searchCities(userInput);
        // Check if result list is empty
        if (results.size() == 0) {
            System.out.println("Leider kein Treffer, versuche es erneut.");
        } else if (results.size() == 1) {
            Weather weatherList = getForecast(results.get(0).getId());
            printForecast(weatherList);
        } else {
            int id = askForInput(results, new Scanner(System.in));
            Weather weatherList = getForecast(id);
            printForecast(weatherList);
        }

        while (true) {
            System.out.println("\n-------------------------------------------\n\nGeben sie einen Städtenamen ein:");

            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            userInput = br.readLine();
            if (userInput.equals(""))
            {
                System.err.println("Ungültige Eingabe");
                continue;
                // ends this iteration of the loop and starts over at while(true)
            }

            results = searchCities(userInput);
            // Check if result list is empty
            if (results.size() == 0) {
                System.out.println("Leider kein Treffer, versuche es erneut.");
            } else if (results.size() == 1) {
                Weather weatherList = getForecast(results.get(0).getId());
                printForecast(weatherList);
            } else {
                int id = askForInput(results, new Scanner(System.in));
                Weather weatherList = getForecast(id);
                printForecast(weatherList);
            }
        }
    }

    /**
     * Searches the City list for matching city names
     * @param userInput the city to search for
     * @return a list of city objects
     * @throws Exception
     */
    public static List<City> searchCities(String userInput) throws Exception {
        List<City> results = new ArrayList<City>(); // Create list for search results
        // Perform search by iterating over all cities
        for (City city : Cities.cities) {
            // Check if current city name contains the speficied user input AND current city country code equals 'de'
            if (city.getName().toLowerCase().contains(userInput.toLowerCase()) &&
                    city.getCountry().toLowerCase().equals("de")) {
                // Then add current city object to result list
                results.add(city);
            }
        }

        return results;
    }

    /**
     * askes the user to select one of the found citys
     * @param results the list of all foun city objects
     * @param scanner a Scanner Object
     * @return the id of the selected city
     * @throws IOException
     */
    public static int askForInput(List<City> results, Scanner scanner) throws IOException {
        int selectedIndex = 0;

        // Iterate over the result list in order to print out the indices and results to the user
        for (int i = 0; i < results.size(); i++) {
            System.out.println((i + 1) + " = " + results.get(i).getName());
        }

        while (true) {
            // Let the user select one of the found citys
            System.out.println("Bitte gültige Eingabe 1 bis " + results.size() + " treffen");

            // Read the user input from the console
            try {
                selectedIndex = scanner.nextInt();
                // Check if the selected index is in a valid range
                if (1 <= selectedIndex && selectedIndex <= results.size()) {
                    // Set the selected city object
                    return results.get(selectedIndex - 1).getId();
                } else {
                    System.err.println("Falsche Eingabe, bitte gebe eine Zahl von 1 bis " + results.size() + "ein.");
                }
            }
            catch (Exception e){
                System.err.println("Falsche Eingabe, bitte gebe eine Zahl von 1 bis " + results.size() + "ein.");
                scanner.next();
            }


        }
    }

    /**
     * Reads the json file
     * @param id the id of the xity
     * @return the weather object created from the json file
     * @throws IOException
     */
    public static de.hdm_stuttgart.mi.sd1.weather.model.Weather getForecast(int id) throws IOException {
        Weather weatherList;

        if (WeatherArchive.checkArchive(id)) {
            weatherList = WeatherArchive.requestFromArchive(id);
        } else {
            WeatherArchive.updateArchiveFromWeb(id);
            weatherList = WeatherArchive.requestFromArchive(id);
        }

        return weatherList;
    }

    /**
     * Prints the weather forecast in the console
     * @param weatherList the weather object created with getForecast
     * @throws IOException
     * @throws ParseException
     */
    public static void printForecast(de.hdm_stuttgart.mi.sd1.weather.model.Weather weatherList) throws IOException, ParseException {
        DateFormat dateAndTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar dateToCheck = Calendar.getInstance();
        Calendar lastCheckedDate = Calendar.getInstance();
        lastCheckedDate.set(1980, 1,1,0,0,0);

        // "list" is an object of type list and contains a smaller list of weatherDataObjects and a Date
        for (de.hdm_stuttgart.mi.sd1.weather.model.List list : weatherList.getList()) {
            dateToCheck.setTime(dateAndTimeFormat.parse(list.getDtTxt()));
            // if the next object in the list is weather info for the next day print the date,
            // if it is weather info for the same day, print only the day
            if (dateToCheck.get(Calendar.DAY_OF_YEAR) > lastCheckedDate.get(Calendar.DAY_OF_YEAR) &&
                    dateToCheck.get(Calendar.YEAR) >= lastCheckedDate.get(Calendar.YEAR)) {
                System.out.print("\n");
                System.out.print(dateToCheck.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.GERMANY) + ", ");
                System.out.printf("%tF", dateToCheck);    // yyyy-MM-dd
                System.out.print("\n");
            }

            // print all the weather info
            for (WeatherData weatherData : list.getWeather()) {
                System.out.print("\t");
                System.out.printf("%tR", dateToCheck);    // HH:mm
                System.out.print(" Uhr ");
                System.out.print(" " + list.getMain().getTemp() + "°C ");
                System.out.print(" " + weatherData.getDescription());
                System.out.print("\n");
            }

            lastCheckedDate.setTime(dateToCheck.getTime());
        }
    }
}
