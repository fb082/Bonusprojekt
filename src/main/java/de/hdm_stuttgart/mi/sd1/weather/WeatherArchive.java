package de.hdm_stuttgart.mi.sd1.weather;

import de.hdm_stuttgart.mi.sd1.weather.model.Weather;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;

/**
 * Is used to check for stored WeatherData, Author Fabian.
 */
public class WeatherArchive {

    /**
     * Standard url for weatherData requests without city id
     * @author Fabian Brecht
     */
    private static URL openWeatherUrl;
    static {
        try
        {
            openWeatherUrl = new URL("https://api.openweathermap.org/data/2.5/forecast?lang=de&" +
                    "APPID=ea93f65b556763b06e4f3aad667042c7&units=metric");

        } catch (MalformedURLException e)
        {
            System.out.print(e.getMessage() + "\n");
            System.out.print(e.getStackTrace() + "\n");
        }
    }

    /**
     * Creates a filename for the WeatherArchive json Files from the city id
     * @author Fabian Brecht
     * @param id the city id
     * @return the matching json file name
     */
    public static String idToFileName(int id) {
        return "archive/W" + id + ".json"; // Example output for id 12345: "src/main/archive/W12345.json"
    }

    /**
     * Checks if a file matching the id is in the Archive and if it
     * is not older than 10 days. Deletes the File if it is too old
     * @author Fabian Brecht
     * @param id
     * @return true, if a matching file not odler than 10 days exists
     */

    public static boolean checkArchive(int id) throws IOException {

        File WeatherFile = new File(WeatherArchive.idToFileName(id));
        Calendar CalExpiryDate = Calendar.getInstance();
        CalExpiryDate.add(Calendar.MINUTE, -10);

        if (WeatherFile.exists()) // If file exists
        {
            if (FileUtils.isFileOlder(WeatherFile, CalExpiryDate.getTime())) // If file is older than 10 days
            {
                FileUtils.forceDelete(WeatherFile); // force deletion of file
                return false;
            }
            return true;
        }
        else return false;
    }

    /**
     * Reads data from openweathermap and stores it in a file. The filename is W[id}.json
     * @author Fabian Brecht
     * @param id city id
     */
    public static void updateArchiveFromWeb(int id) {
        try
        {
            // Creates a new url and adds the city id to it
            URL cityIdUrl = new URL(
                    openWeatherUrl.getProtocol(),
                    openWeatherUrl.getHost(),
                    openWeatherUrl.getPort(),
                    openWeatherUrl.getFile()+ "&id=" + id);

            // Reads the json data from the url and saves it in file W[id].json
            FileUtils.copyURLToFile(cityIdUrl, new File(WeatherArchive.idToFileName(id)));
        }
        catch (Exception e)
        {
            System.out.print(e.getMessage() + "\n");
            System.out.print(e.getStackTrace() + "\n");
        }
    }

    /**
     * Reads the json file and returns the weather object
     * @author Fabian Brecht
     * @param id the city id
     * @return the weather object
     * @throws Exception
     */
    public static Weather requestFromArchive(int id) throws IOException {
        File WeatherFile = new File(WeatherArchive.idToFileName(id));
        if (!WeatherFile.exists()) throw new FileNotFoundException();

        return WeatherDataParser.parseFileToObject(WeatherArchive.idToFileName(id));
    }
}
