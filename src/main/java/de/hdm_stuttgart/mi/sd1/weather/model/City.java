// Generated by http://www.jsonschema2pojo.org

package de.hdm_stuttgart.mi.sd1.weather.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
  "id",
  "name",
  "coord",
  "country",
  "population"
})
public class City {

  @JsonProperty("id")
  private int id;
  @JsonProperty("name")
  private String name;
  @JsonProperty("coord")
  private Coord coord;
  @JsonProperty("country")
  private String country;
  @JsonProperty("population")
  private int population;

  /**
   * No args constructor for use in serialization
   */
  public City() {
  }

  /**
   * @param coord
   * @param id
   * @param name
   * @param population
   * @param country
   */
  public City(int id, String name, Coord coord, String country, int population) {
    super();
    this.id = id;
    this.name = name;
    this.coord = coord;
    this.country = country;
    this.population = population;
  }

  @JsonProperty("id")
  public int getId() {
    return id;
  }

  @JsonProperty("id")
  public void setId(int id) {
    this.id = id;
  }

  @JsonProperty("name")
  public String getName() {
    return name;
  }

  @JsonProperty("name")
  public void setName(String name) {
    this.name = name;
  }

  @JsonProperty("coord")
  public Coord getCoord() {
    return coord;
  }

  @JsonProperty("coord")
  public void setCoord(Coord coord) {
    this.coord = coord;
  }

  @JsonProperty("country")
  public String getCountry() {
    return country;
  }

  @JsonProperty("country")
  public void setCountry(String country) {
    this.country = country;
  }

  @JsonProperty("population")
  public int getPopulation() {
    return population;
  }

  @JsonProperty("population")
  public void setPopulation(int population) {
    this.population = population;
  }

}
